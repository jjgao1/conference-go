from django.http import JsonResponse

from .models import Presentation

from common.json import ModelEncoder

from django.views.decorators.http import require_http_methods

import json


class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "title",
    ]

class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
    ]

def api_list_presentations(request, conference_id):

    # presentations = [
    #     {
    #         "title": p.title,
    #         "status": p.status.name,
    #         "href": p.get_api_url(),
    #     }
    #     for p in Presentation.objects.filter(conference=conference_id)
    # ]
    # return JsonResponse({"presentations": presentations})
    presentations = Presentation.objects.filter(conference=conference_id)
    return JsonResponse(
        {"presentations": presentations},
        encoder=PresentationListEncoder
        )

    def get_extra_data(self, o):
        return {"status": o.status.name}


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_presentation(request, id):
    if request.method =="GET":
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
                presentation,
                encoder=PresentationDetailEncoder,
                safe=False
            )
    elif request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count})
